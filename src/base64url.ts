import base64js from "base64-js";

/** Converts b64 into text. */
function toText(b64: string): string {
  const bytes = toByteArray(b64);
  return new TextDecoder().decode(bytes);
}

/** Converts text into b64url. */
function fromText(text: string): string {
  const bytes = new TextEncoder().encode(text);
  return fromByteArray(bytes);
}

/** Converts a b64url into a byte array. */
function toByteArray(b64: string): Uint8Array {
  return base64js.toByteArray(padEquals(b64));
}

/** Converts a byte array into a b64url. */
function fromByteArray(uint8: Uint8Array): string {
  return urlSafe(base64js.fromByteArray(uint8));
}

/** Pads missing equal (`=`) signs to the end of a base64 string. */
function padEquals(b64: string): string {
  // https://github.com/beatgammit/base64-js/issues/45#issuecomment-1066111486
  if((b64.length % 4) !== 0) {
    b64 += "=".repeat(4 - (b64.length % 4));
  };

  return b64;
}

/** Converts b64 to b64url. */
function urlSafe(b64: string): string {
  return b64
    // https://github.com/blakeembrey/universal-base64url/blob/master/src/index.ts
    .replace(/\//g, "_")
    .replace(/\+/g, "-")
    .replace(/=+$/, "");
}

export {
  toText,
  fromText,
  toByteArray,
  fromByteArray,
}