import { toByteArray, fromByteArray, toText, fromText } from "./base64url";

/**
 * Decodes the URL and verifies its signature.
 * Returns the decoded URL, or throws if the signature doesn't match.
 */
async function decodeAndVerifyUrl(url: string, secretKey: string, algorithm = "SHA-256"): Promise<string> {
  const { pathname } = new URL(url);
  const [_, _proxy, sig64, url64] = pathname.split("/");
  
  const key = await getKey(secretKey, algorithm);
  const sig = toByteArray(sig64);
  const url64Bytes = new TextEncoder().encode(url64);
  
  const verified = await crypto.subtle.verify(
    "HMAC",
    key,
    sig,
    url64Bytes,
  );

  if (verified) {
    return toText(url64);
  } else {
    throw Error("Signature did not match!");
  }
};

/** Get an HMAC CryptoKey from a secret string. */
function getKey(secretKey: string, algorithm: string): Promise<CryptoKey> {
  const rawKey = new TextEncoder().encode(secretKey);

  return crypto.subtle.importKey(
    "raw",
    rawKey,
    { name: "HMAC", hash: algorithm },
    false,
    ["sign", "verify"],
  );
};

/** Sign a URL and return the generated pathname. */
async function signUrl(url: string, secretKey: string, algorithm: string): Promise<string> {
  const key = await getKey(secretKey, algorithm);

  const url64 = fromText(url);
  const url64Bytes = new TextEncoder().encode(url64);

  const sig = await crypto.subtle.sign(
    "HMAC",
    key,
    url64Bytes,
  );

  const sig64 = fromByteArray(new Uint8Array(sig));

  return `/proxy/${sig64}/${url64}/`;
};

export {
  decodeAndVerifyUrl,
  signUrl,
};