import { decodeAndVerifyUrl, signUrl } from "./crypto";

/**
 * Digest algorithm used to sign/verify URLs.
 * @see https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest#supported_algorithms
 */
type Algorithm = "SHA-1" | "SHA-256" | "SHA-384" | "SHA-512";

interface Env {
  /** Secret key used to decode URLs. */
  SECRET_KEY?: string;
  /** Digest algorithm used to sign/verify URLs. */
  ALGORITHM?: Algorithm | string;
}

/** Check whether the request URL matches the expected format. */
function validRequest(url: string): boolean {
  const [slash, proxy, sig64, url64] = new URL(url).pathname.split("/");

  if (slash === "" && proxy === "proxy" && sig64 !== undefined && url64 !== undefined) {
    return true;
  } else {
    return false;
  }
}

/** Handle requests to the worker. */
async function handleFetch(
  request: Request,
  env: Env,
  _ctx: ExecutionContext
): Promise<Response> {
  if (!env.SECRET_KEY) {
    console.error("SECRET_KEY is not set.")
    return new Response("Internal server error.", { status: 500 });
  }

  if (validRequest(request.url)) {
    try {
      const url = await decodeAndVerifyUrl(request.url, env.SECRET_KEY, env.ALGORITHM);
      return fetch(url);
    } catch(e) {
      console.error(e);
      return new Response("Invalid signature.", { status: 422 });
    }
  } else {
    return new Response("Not found.", { status: 404 });
  }
};

export default {
  fetch: handleFetch,
};
